import { Image, Platform } from "react-native";
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { COLORS } from "../../core/theme";
import icons from "../../core/icons";
import {
  ProfileScreen
} from "../../screens";

import AdminHomeScreen from "../../screens/Admin/AdminHomeScreen";
import AdminMaquinasScreen from "../../screens/Admin/Maquinas/AdmiMaquinasScreen";
import AdminHerramientasScreen from "../../screens/Admin/AdminHerramientasScreen";
import AdminUsuariosScreen from "../../screens/Admin/Usuarios/AdmiUsuariosScreen";
import AgregarUsuariosScreen from "../../screens/Admin/Usuarios/AgregarUsuariosScreen";
import EditarUsuarioScreen from "../../screens/Admin/Usuarios/EditarUsuarioScreen";
import GestionarUsuariosScreen from '../../screens/Admin/Usuarios/GestionarUsuariosScreen'
import AsignarDepartamentoSupervisor from '../../screens/Admin/Departamentos/AsignarDepartamentoSupervisor'
import AdminDepartamentosScreen from '../../screens/Admin/Departamentos/AdminDepartamentosScreen'
import GestionarDepartamento from '../../screens/Admin/Departamentos/GestionarDepartamento'


const Stack = createStackNavigator();

const AdminStack = () => {
  return (
    <Stack.Navigator
    screenOptions={({ route }) => ({
      headerShown: route.name !== 'Home', 
      headerStyle: {
        backgroundColor: COLORS.white,
      }
    })}
    
    >
      <Stack.Screen name="Home" component={AdminTabNavigation} />
      <Stack.Screen name="Maquinas" component={AdminMaquinasScreen} />
      <Stack.Screen name="Usuarios" component={AdminUsuariosScreen} />
      <Stack.Screen name="Agregar Usuario" component={AgregarUsuariosScreen} />
      <Stack.Screen name="Gestionar Usuario" component={GestionarUsuariosScreen} />
      <Stack.Screen name="Editar Usuario" component={EditarUsuarioScreen} />
      <Stack.Screen name="Departamentos" component={AdminDepartamentosScreen} />
      <Stack.Screen name="Asignar Departamento" component={AsignarDepartamentoSupervisor} />
      <Stack.Screen name="Gestionar Departamento" component={GestionarDepartamento} />
      <Stack.Screen name="Herramientas" component={AdminHerramientasScreen} />
    </Stack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

const screenOptions = {
  tabBarShowLabel: true,
  headerShown: false,
  tabBarStyle: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    elevation: 0,
    height: Platform.OS === "ios" ? 90 : 55,
    backgroundColor: COLORS.white,
  },
  tabBarLabelStyle: { 
    fontSize: 12,
    fontFamily: 'Gilroy-Bold',
    marginTop: -3,
    marginBottom: 5,
  },
  tabBarActiveTintColor: '#133036'
};

const AdminTabNavigation = () => {
  return (
    <Tab.Navigator screenOptions={screenOptions}>
      <Tab.Screen
        name="AdminHomeScreen"
        component={AdminHomeScreen}
        options={{
          tabBarLabel: () => null,
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={focused ? icons.solicitud : icons.solicitudOutline}
                resizeMode="contain"
                style={{
                  height: 24,
                  width: 24,
                  tintColor: focused ? COLORS.primary : COLORS.black,
                }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          tabBarLabel: () => null,
          tabBarIcon: ({ focused }) => {
            return (
              <Image
                source={focused ? icons.usuario : icons.usuarioOutline}
                resizeMode="contain"
                style={{
                  height: 24,
                  width: 24,
                  tintColor: focused ? COLORS.primary : COLORS.black,
                }}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default AdminStack;