import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Alert } from "react-native";
import Axios from 'axios';
import { SERVER_IP } from "../../../config";

const url = `http://${SERVER_IP}:8000/api/users`;

export default class GestionarUsuariosScreen extends Component {
    state = {
        id: null,
        nombrePila: '',
        apPat: '',
        apMat: '',
        rol: ''
    };

    componentDidMount() {
        const { route } = this.props;
        const { id } = route.params;
        this.obtenerUsuario(id);
    }
    
    obtenerUsuario = async (id) => {
        try {
            const response = await Axios.get(`${url}/${id}`);
            const userData = response.data.body[0]; 
            this.setState({
                id: userData.id,
                nombrePila: userData.nombrePila,
                apPat: userData.apPat,
                apMat: userData.apMat,
                rol: userData.rol,
                
            });
        } catch (error) {
            console.error('Error al obtener usuario:', error);
        }
    };

    manejarCambioTexto = (campo, valor) => {
        this.setState({ [campo]: valor });
    };


    confirmarDeshabilitarUsuario = () => {
        Alert.alert(
            'Confirmar',
            '¿Estás seguro de que deseas deshabilitar este usuario?',
            [
                {text: 'Cancelar', style: 'cancel'},
                {text: 'Sí', onPress: this.deshabilitarUsuario}
            ],
            { cancelable: false }
        );
    };


    deshabilitarUsuario = async () => {
        try {
            await Axios.put(`${url}`, {
                id: this.state.id,
            });
            this.props.navigation.navigate('Usuarios');
        } catch (error) {
            console.error('Error al deshabilitar usuario:', error);
        }
    };
    

    obtenerEtiquetaRol = (rol) => {
        const etiquetasRoles = {
            'empleado_mantenimiento': 'Empleado',
            'administrador': 'Administrador',
            'supervisor': 'Supervisor'
        };
        return etiquetasRoles[rol] || rol; 
    };

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Detalles del Usuario</Text>
                <View style={styles.infoContainer}>
                    <Text style={styles.label}>ID:</Text>
                    <Text>{this.state.id}</Text>
                    <Text style={styles.label}>Nombre:</Text>
                    <Text>{this.state.nombrePila}</Text>
                    <Text style={styles.label}>Apellido Paterno:</Text>
                    <Text>{this.state.apPat}</Text>
                    <Text style={styles.label}>Apellido Materno:</Text>
                    <Text>{this.state.apMat}</Text>
                    <Text style={styles.label}>Rol:</Text>
                    <Text>{this.obtenerEtiquetaRol(this.state.rol)}</Text>
                </View>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={[styles.button, { backgroundColor: 'red' }]}
                        onPress={this.confirmarDeshabilitarUsuario}
                    >
                        <Text style={styles.buttonText}>Deshabilitar</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.button, { backgroundColor: '#007AFF' }]}
                        onPress={() => navigation.navigate('Editar Usuario', {
                            id: this.state.id,
                            nombrePila: this.state.nombrePila,
                            apPat: this.state.apPat,
                            apMat: this.state.apMat,
                            rol: this.state.rol,
                            obtenerUsuario: this.obtenerUsuario 
                        })}
                    >
                        <Text style={styles.buttonText}>Editar</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        padding: 20,
        paddingTop: 50,
    },
    header: {
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: 'center',
        color: 'black',
        fontFamily: 'Gilroy-Bold',
    },
    infoContainer: {
        width: '80%',
        padding: 20,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        marginBottom: 20,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 4,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        marginTop: 20,
        alignItems: 'center',
    },
    button: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#007AFF',
        marginHorizontal: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 4,
        elevation: 1,
        marginBottom: 20,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
        fontFamily: 'Gilroy-Bold',
    },
    label: {
        fontWeight: 'bold',
        marginBottom: 5,
    }    
});
