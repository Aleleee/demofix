import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { SERVER_IP } from "../../config";

const ProfileScreen = ({ navigation }) => {
  const [userInfo, setUserInfo] = useState(null);

  const handleLogout = async () => {
    Alert.alert(
      "Cerrar sesión",
      "¿Estás seguro de que quieres cerrar sesión?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Sí",
          onPress: logout,
        },
      ]
    );
  };

  const logout = async () => {
    try {
      await AsyncStorage.multiRemove(["userToken", "userId"]);
      navigation.navigate("LoginScreen");
    } catch (e) {
      console.error("Error al limpiar el token de autenticación:", e);
    }
  };

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const token = await AsyncStorage.getItem("userToken");
        const userId = await AsyncStorage.getItem("userId");

        if (!token || !userId) {
          // console.error(
          //   "Token o ID de usuario no encontrado. El usuario no está autenticado."
          // );
          navigation.navigate("LoginScreen");
          return;
        }

        const response = await axios.get(
          `http://${SERVER_IP}:8000/api/users/${userId}`,
          {
            headers: { Authacorization: `Bearer ${token}` },
          }
        );

        if (response.data) {
          setUserInfo(response.data);
        } else {
          console.error("No se pudo obtener la información del usuario");
        }
      } catch (error) {
        console.log("Error al obtener la información del usuario:", error);
      }
    };

    fetchUserInfo();
  }, [navigation]);

  if (!userInfo) {
    return (
      <Background>
        <Text>Cargando perfil...</Text>
      </Background>
    );
  }

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Mi Perfil</Header>
      </View>
      <ScrollView style={styles.container}>
        <View style={styles.profileCard}>
          <Image
            source={require("../../assets/empmante.png")}
            style={styles.profileImage}
          />
          <Text style={styles.title}>Número de empleado:</Text>
          <Text style={styles.description}>
            {userInfo?.body[0].id || "Usuario"}
          </Text>
          <Text style={styles.title}>Nombre completo:</Text>
          <Text style={styles.description}>
            {`${userInfo?.body[0].nombrePila || ""} ${
              userInfo?.body[0].apPat || ""
            } ${userInfo?.body[0].apMat || ""}`.trim()}
          </Text>
          <TouchableOpacity style={styles.button} onPress={handleLogout}>
            <Text style={styles.buttonText}>Cerrar sesión</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 70,
  },
  profileCard: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginBottom: 16,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 1,
    alignItems: "center",
  },
  profileImage: {
    width: 256,
    height: 256,
    resizeMode: "cover",
    marginBottom: 50,
  },
  title: {
    fontFamily: "Gilroy-Bold",
    fontSize: 20,
    color: "#133036",
    marginBottom: 10,
  },
  description: {
    fontFamily: "Gilroy-Regular",
    fontSize: 18,
    color: "#333",
    marginBottom: 10,
  },
  button: {
    backgroundColor: "#3DC253",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    width: '80%'
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    fontFamily: "Gilroy-Bold",
  },
  // HEADER
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: "#133036",
  },
  header: {
    fontSize: 25,
    fontFamily: "Gilroy-Bold",
    color: "white",
  },
});

export default ProfileScreen;
