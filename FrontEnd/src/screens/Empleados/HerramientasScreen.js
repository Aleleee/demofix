import React, { useState } from 'react';
import { Alert, StyleSheet, View, Text, TouchableOpacity, FlatList, Image } from 'react-native';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Icon from 'react-native-vector-icons/FontAwesome';

const HerramientasScreen = ({ onConfirm, navigation }) => {
  const [tools, setTools] = useState([
    { id: '1', name: 'Martillo', quantity: 0, image: require('../../assets/icons/herramienta.png') },
    { id: '2', name: 'Destornillador', quantity: 0, image: require('../../assets/icons/herramienta.png') }
  ]);

  const incrementQuantity = id => {
    setTools(currentTools =>
      currentTools.map(tool => (tool.id === id ? { ...tool, quantity: tool.quantity + 1 } : tool))
    );
  };

  const decrementQuantity = id => {
    setTools(currentTools =>
      currentTools.map(tool => (tool.id === id && tool.quantity > 0 ? { ...tool, quantity: tool.quantity - 1 } : tool))
    );
  };

  const renderTool = ({ item }) => (
    <View style={styles.card}>
      <Image source={item.image} style={styles.toolImage} />
      <View style={styles.toolDetails}>
        <Text style={styles.toolName}>{item.name}</Text>
        <View style={styles.toolControls}>
          <TouchableOpacity style={styles.controlButton} onPress={() => decrementQuantity(item.id)}>
            <Icon name="minus" size={16} color="#333" />
          </TouchableOpacity>
          <Text style={styles.toolQuantity}>{item.quantity}</Text>
          <TouchableOpacity style={styles.controlButton} onPress={() => incrementQuantity(item.id)}>
            <Icon name="plus" size={16} color="#333" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  return (
    <Background>
      <View style={styles.headerContainer}>
        <Header style={styles.header}>Herramientas</Header>
      </View>
      <FlatList data={tools} renderItem={renderTool} keyExtractor={item => item.id} style={styles.list} />
      <TouchableOpacity style={styles.confirmButton} onPress={onConfirm}>
        <Text style={styles.confirmButtonText}>Confirmar</Text>
      </TouchableOpacity>
    </Background>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 120,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: '#133036',
  },
  header: {
    fontSize: 25,
    fontFamily: 'Gilroy-Bold',
    color: 'white',
  },
  list: {
    flex: 1,
  },
  card: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 4,
  },
  toolImage: {
    width: 64,
    height: 64,
    borderRadius: 32,
  },
  toolDetails: {
    marginLeft: 16,
    flex: 1,
    justifyContent: 'center',
  },
  toolName: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333',
  },
  toolControls: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  controlButton: {
    backgroundColor: '#F0F0F0',
    borderRadius: 15,
    padding: 8,
    marginHorizontal: 4,
  },
  toolQuantity: {
    fontSize: 16,
  },
});

export default HerramientasScreen;