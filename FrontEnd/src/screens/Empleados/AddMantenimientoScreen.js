import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import Background from '../../components/Background';
import BackButton from '../../components/BackButton';
import Header from '../../components/Header';

const AddMantenimientoScreen = ({ navigation }) => {
  const [tipoMantenimiento, setTipoMantenimiento] = useState('');
  const [lugar, setLugar] = useState('');
  const [numeroMaquina, setNumeroMaquina] = useState('');
  const [fechaInicio, setFechaInicio] = useState('');
  const [nombreEmpleado, setNombreEmpleado] = useState('');
  const [herramientas, setHerramientas] = useState('');
  const [estado, setEstado] = useState('');
  const [fechaFinal, setFechaFinal] = useState('');
  const [conclusion, setConclusion] = useState('');

  const confirmarMantenimiento = () => {
    // Aquí la lógica para confirmar el mantenimiento
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <Header title="Agregar Mantenimiento" />
      <ScrollView style={styles.container}>
        <TextInput
          placeholder="Tipo de mantenimiento"
          value={tipoMantenimiento}
          onChangeText={setTipoMantenimiento}
          style={styles.input}
        />
        <TextInput
          placeholder="Lugar"
          value={lugar}
          onChangeText={setLugar}
          style={styles.input}
        />
        <TextInput
          placeholder="Estado"
          value={estado}
          onChangeText={setEstado}
          style={styles.input}
        />
        <TextInput
          placeholder="Fecha final"
          value={fechaFinal}
          onChangeText={setFechaFinal}
          style={styles.input}
        />
        <TextInput
          placeholder="Conclusión del trabajo"
          value={conclusion}
          onChangeText={setConclusion}
          style={styles.input}
          multiline
        />
        <TouchableOpacity style={styles.button} onPress={confirmarMantenimiento}>
          <Text style={styles.buttonText}>Confirmar Mantenimiento</Text>
        </TouchableOpacity>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  button: {
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
    marginTop: 20,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default AddMantenimientoScreen;
