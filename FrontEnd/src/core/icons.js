const arrowLeft = require("../assets/icons/arrow-left.png");
const mantenimiento = require("../assets/icons/mantenimiento.png");
const mantenimientoOutline = require("../assets/icons/mantenimiento-outline.png")
const solicitud = require("../assets/icons/solicitud.png");
const solicitudOutline = require("../assets/icons/solicitud-outline.png");
const tarea = require("../assets/icons/tarea.png")
const tareaOutline = require("../assets/icons/tarea-outline.png")
const usuario = require("../assets/icons/usuario.png")
const usuarioOutline = require("../assets/icons/usuario-outline.png")
const escaner = require("../assets/icons/escaner.png")
const escanerOutline = require("../assets/icons/escaner-outline.png")
const anadir = require("../assets/icons/anadir.png")
const anadirOutline = require("../assets/icons/anadir-outline.png")
const crearmantenimiento = require("../assets/icons/crearmantenimiento.png")
const crearmantenimientoOutline = require("../assets/icons/crearmantenimiento-outline.png")
const check = require("../assets/icons/check.png")
const checkOutline = require("../assets/icons/check-outline.png")


export default {
    arrowLeft,
    solicitud,
    solicitudOutline,
    mantenimiento,
    mantenimientoOutline,
    tarea,
    tareaOutline,
    usuario,
    usuarioOutline,
    escaner,
    escanerOutline,
    anadir,
    anadirOutline,
    crearmantenimiento,
    crearmantenimientoOutline,
    check,
    checkOutline
}